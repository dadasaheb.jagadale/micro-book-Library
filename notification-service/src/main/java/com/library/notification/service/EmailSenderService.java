package com.library.notification.service;

import com.library.notification.dto.EmailSender;

public interface EmailSenderService {

    String sendEmail(EmailSender sender);
}
