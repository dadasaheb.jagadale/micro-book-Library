package com.library.notification.service;

import com.library.notification.dto.EmailSender;
import com.library.notification.utils.FmTemplateToStringConverterUtils;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    @Autowired
    private FmTemplateToStringConverterUtils fmTemplateToStringConverterUtils;

    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;

    /**
     * @param sender
     * @return
     */
    @Override
    public String sendEmail(EmailSender sender) {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper;

        try {
            Map<String, String> data = fmTemplateToStringConverterUtils.getEmailBody(sender);
            helper = new MimeMessageHelper(message, false);
            helper.setFrom(from);
            helper.setTo(sender.getTo().toArray().toString());
            helper.setSubject(data.get("subject"));
            helper.setText(data.get("emailBody"), true);
            mailSender.send(message);
            return "Mail sent Successfully";

        } catch (MessagingException e) {
            e.printStackTrace();
            return "Error while sending mail!!!";
        }
    }
}
