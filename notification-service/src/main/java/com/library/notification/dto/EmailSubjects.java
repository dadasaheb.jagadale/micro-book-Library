package com.library.notification.dto;

import lombok.Getter;

@Getter
public class EmailSubjects {
    private static final String registerBookInLibrary="Thank You for Your Generous Book Contribution!!";
    private static final String borrowBookFromLibrary="Confirmation of Book Borrowing";
    private static final String returnBookToLibrary="Book Return Confirmation";
    private static final String removeBookFromLibrary="Borrowing Your Generously Provided Books";
    private static final String registerReader="Welcomes you to Micro Library Platform!";
    private static final String removeReader="Account Deactivation from Micro Library Platform";

}
