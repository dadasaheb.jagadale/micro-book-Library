package com.library.notification.dto;

import lombok.Builder;

import java.util.List;

@Builder
public class Book {

    private String title;
    private List<String> author;
    private String publicationYear;
    private String isbn;
    private double price;
    private String language;
    private int pageCount;
    private String description;

}
