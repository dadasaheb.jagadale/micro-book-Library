package com.library.notification.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailSender {

    private String name;
    private List<String> to;
    private String eventType;
    private List<Book> books;
    private String submissionDueDate;
    private String actualSubmissionDate;
    private String registerDate;
    private String libraryCardNo;
    private String closureDate;
}
