package com.library.notification.controller;

import com.library.notification.dto.EmailSender;
import com.library.notification.service.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailSenderController {

    @Autowired
    private EmailSenderService emailSenderService;

    @PostMapping(value = "sendEmail")
    public ResponseEntity<String> sendEmail(@RequestBody EmailSender emailSender){

        String response=emailSenderService.sendEmail(emailSender);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "sendEmailWithAttachments")
    public ResponseEntity<String> sendEmailWithAttachments(){

        String response=null;
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}