package com.library.notification.utils;

import com.library.notification.dto.EmailSender;
import com.library.notification.dto.EmailSubjects;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Component
public class FmTemplateToStringConverterUtils {

    @Autowired
    private Configuration configuration;

    public Map<String, String> getEmailBody(EmailSender sender) {

        String emailBody = null;
        Map<String, Object> model = new HashMap<>();
        Map<String, String> out = new HashMap<>();
        try {
            String templateName = sender.getEventType();
            EmailSubjects emailSubjects = new EmailSubjects();
            Class<?> subjectClass = emailSubjects.getClass();
            Field titleField = subjectClass.getDeclaredField(templateName);
            titleField.setAccessible(true);
            String subject = (String) titleField.get(emailSubjects);

            Template template = configuration.getTemplate(templateName + ".ftl");
            model.put("name", sender.getName());
            model.put("books", sender.getBooks());
            out.put("subject", subject);
            emailBody = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
            out.put("emailBody", emailBody);

        } catch (NoSuchFieldException | IllegalAccessException | IOException | TemplateException e) {

        }
        return out;
    }
}
