<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome to [Library Name]!</title>
</head>
<body>
<h3>Hi ${name}!!</h3>
<div>
    We are thrilled to welcome you to [Library Name]! Your library account has been successfully created, and you can now start enjoying our vast collection of books and resources.<br>
    Here are your account details:<br>
    <!--        Library Card Number: [Library Card Number]-->
    <!--        Username: [Username]-->
    <ul>
        <#list books as book>
        <li>${book}</li>
    </#list>
    </ul>
    Thank you for choosing [Library Name]. We look forward to being a part of your reading and learning journey!<br><br>
    Regards,<br>
    Team Micro Library
</div>
</body>
</html>