<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Confirmation of Book Borrowing</title>
  </head>
  <body>
    <h3>Hi ${name}!!</h3>
	<div>
        We are pleased to inform you that you have successfully checked out the following book from Library:<br>
        <ul>
            <#list books as book>
                <li>${book}</li>
            </#list>
        </ul>
<!--        Title: [Book Title]-->
<!--        Author: [Author's Name]-->
<!--        Due Date: [Due Date]-->
<!--        -->
        Please take note of the due date mentioned above. Kindly return the book to the library by this date to avoid any overdue fees.<br>
        If you have any questions or need to renew the book, please feel free to contact our library staff at [Library Contact Information]<br>
        Happy reading!<br><br>
        Regards,<br>
        Team Micro Library
	</div>
  </body>
</html>