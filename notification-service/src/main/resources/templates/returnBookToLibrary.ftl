<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Book Return Confirmation</title>
  </head>
  <body>
    <h3>Hi ${name}!!</h3>
	<div>
        We wanted to confirm that we have received the book you recently returned to Library. We appreciate your promptness in returning the book.<br>
        Here are the details of the returned book:<br>
        <ul>
            <#list books as book>
                <li>${book}</li>
            </#list>
        </ul>
<!--        Title: [Book Title]-->
<!--        Author: [Author's Name]-->
<!--        Date Returned: [Date of Return]-->
        The book has been checked in and is now available for other patrons to borrow. We trust you had a rewarding reading experience.<br>
        Thank you for using our library services. We look forward to serving you again in the future.<br><br>
        Regards,<br>
        Team Micro Library
	</div>
  </body>
</html>