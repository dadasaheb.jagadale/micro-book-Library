<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Account Removal from [Library Name]</title>
  </head>
  <body>
    <h3>Hi ${name}!!</h3>
	<div>
        We hope this email finds you well. We regret to inform you that your account with [Library Name] has been removed from our system, per your request.<br>
        Your account details have been deactivated, and you will no longer have access to our library's resources and services<br>
        Thank you for being a part of [Library Name], and we wish you all the best in your future endeavors.<br><br>
        Regards,<br>
        Team Micro Library
	</div>
  </body>
</html>