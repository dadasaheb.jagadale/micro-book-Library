<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Borrowing Your Generously Provided Books</title>
  </head>
  <body>
    <h3>Hi ${name}!!</h3>
	<div>
        I trust this email finds you well. We wanted to inform you that the time span you dedicated for your kind book contribution to [Library Name] has commenced, and we're pleased to let you know that the books you shared are now available for you to borrow from the library.<br>
        Here are the details of the books you provided, which you are now borrowing:<br>
<!--        Title: [Book Title]-->
<!--        Author: [Author's Name]-->
<!--        Time Span: [Start Date] to [End Date]-->
        We wholeheartedly appreciate your continued engagement and support for [Library Name]. Your contributions contribute to the vibrancy of our library and its mission to inspire lifelong learning.<br><br>
        Regards,<br>
        Team Micro Library
	</div>
  </body>
</html>