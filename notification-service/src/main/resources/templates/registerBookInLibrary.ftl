<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Thank You for Your Generous Book Contribution</title>
  </head>
  <body>
    <h3>Hi ${name}!!</h3>
	<div>
        I hope this email finds you well. On behalf of [Library Name], I wanted to extend our heartfelt gratitude for your recent contribution of books to our library<br>
        Your generosity in sharing your collection with our community is truly commendable and greatly appreciated. The books you have provided will undoubtedly enrich the reading experience of our library patrons and contribute to their learning and enjoyment.<br>
        Here are the details of the books you have shared with us:
        <!--        Title: [Book Title]-->
<!--        Author: [Author's Name]-->
<!--        Quantity: [Number of Copies]-->
        Thank you for making a meaningful impact on [Library Name] and the readers who benefit from our services. We look forward to continuing our partnership in promoting the joy of reading and learning.<br><br>
        Regards,<br>
        Team Micro Library
	</div>
  </body>
</html>