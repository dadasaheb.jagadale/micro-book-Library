package com.library.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class LibraryAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryAdminApplication.class, args);
	}

}
