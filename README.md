# Micro Book Library Management system

## Book Orchestration Service
| service name               | swagger url                                 | service port | use                        |
|----------------------------|---------------------------------------------|--------------|----------------------------|
| book-orchestration-service | http://localhost:9092/swagger-ui/index.html | 9092         | Book Orchestration Service |

## Library Admin Service
| service name          | swagger url                                 | service port | use                    |
|-----------------------|---------------------------------------------|--------------|------------------------|
| library-admin-service | http://localhost:9093/swagger-ui/index.html | 9093         | Library Admin Service  |

## Notification Service
| service name         | swagger url                                 | service port | use                    |
|----------------------|---------------------------------------------|--------------|------------------------|
| notification-service | http://localhost:8761/swagger-ui/index.html | 9094         | Notification Service   |

## Eureka Discovery Service
| service name              | service url           | service port | use                        |
|---------------------------|-----------------------|--------------|----------------------------|
| eureka-discovery-service  | http://localhost:8761 | 9092         | Eureka Discovery Service   |

## Api Gateway Service
| service name        | service url           | service port | use                   |
|---------------------|-----------------------|--------------|-----------------------|
| api-gateway-service | http://localhost:8761 | 9091         | Api Gateway Service   |