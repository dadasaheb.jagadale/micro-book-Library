package com.library.bookorchestration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class BookOrchestrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookOrchestrationApplication.class, args);
	}

}
